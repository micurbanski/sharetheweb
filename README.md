# ShareTheWeb #

Social Web application for sharing images with bookmarklet for quick image sharing

### Features ###

* Quick Image share with bookmarlet 
* Like/Unlike, Follow/Unfollow other users activity stream
* Dynamic images view count (Redis)
* AJAX pagination
* Automatic thumbnail generation (sorl-thumbnail)
* Facebook Authentication (python-social-auth)

### Requirements ###

* Python 3.3+
* requirements.txt
* [Redis 3.0.4](http://redis.io)