# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('actions', '0002_action_total_likes'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='action',
            name='total_likes',
        ),
    ]
