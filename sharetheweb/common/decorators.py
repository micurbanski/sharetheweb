from django.http import HttpResponseBadRequest


def ajax_required(foo):
    """
    decorator for forcing only ajax requests through like/unlike mechanism
    :param foo:
    :return:
    """
    def wrap(request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest
        return foo(request, *args, **kwargs)
    wrap.__doc__ = foo.__doc__
    wrap.__name__ = foo.__name__
    return wrap
