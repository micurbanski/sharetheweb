from django.contrib.auth.models import User
from django import forms
from .models import Profile


class LoginForm(forms.Form):
    """
    Class for Login Form using django auth
    """
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class RegistrationForm(forms.ModelForm):
    """
    Custom Form for User Registration
    """
    class Meta:
        model = User
        fields = ('username', 'first_name', 'email')

    password = forms.CharField(widget=forms.PasswordInput,
                               label='Password')
    password2 = forms.CharField(widget=forms.PasswordInput,
                                label='Repeat Password')

    def clean_password2(self):
        clean_data = self.cleaned_data
        if clean_data['password'] != clean_data['password2']:
            raise forms.ValidationError('Passwords do not match')
        return clean_data['password2']


class UserEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class ProfileEditForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('date_of_birth', 'photo')
